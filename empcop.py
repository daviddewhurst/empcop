import functools
from collections.abc import Sequence

import numpy as np
from scipy import stats
from sklearn.neighbors import BallTree


class EmpiricalCopula(Sequence):
    """
    Constructs an empirical copula from data.

    :param data: array of datapoints, shape (N, p)
    :type data: `numpy.ndarray`
    :param dataname: name of dataset, optional
    :type dataname: `str`
    :param varnames: name of variables, optional
    :type varnames: `list` of `str`
    """

    def __init__(
            self, 
            data, 
            dataname=None,
            varnames=None,
            ):
        super(EmpiricalCopula, self).__init__()
        if self._check_data(np.array(data)):
            self.data = data
        else:
            raise ValueError('Error in data input. Data should be 2d numeric castable to numpy array')
        self.dataname = dataname
        self.varnames = varnames

        self.marginal_pdf_bins = None
        self.marginal_pdf_edges = None
        self.marginal_pdfs = None
        self.marginal_unifs = None
        self.new_univariate_dists = None
        self.replaced_univariate_dists = None
        self.copula_pdf_tree = None

        # set up the rv objects
        self._construct_marginal_pdfs()
        self._construct_marginal_unifs()
        self._construct_copula_pdf()
        self.active_dists = self.marginal_pdfs
        self.shape = self.marginal_unifs.shape[0]

    def __getitem__(self, inds):
        return self.marginal_unifs[:, inds]

    def __len__(self):
        return self.marginal_unifs.shape[0]

    def _check_data(self, data):
        if len(data.shape) != 2:
            return False
        return True

    def _check_iter(self, obj):
        try:
            _ = iter(obj)
        except TypeError:
            return False
        else:
            return True

    def log_pdf(
            self,
            pts,
            ):
        """
        Compute the joint log probability of each datapoint using the empirical copula and active dists.

        :param pts: the points for which to compute the log pdf, shape (n, p)
        :type pts: `numpy.ndarray`

        :returns: `numpy.ndarray` of shape (n,), the values of the logpdf at each point
        """
        # first compute unifs using the active dists
        # and calculate the log jacobian factor
        # this is the sum of log pdfs of the marginals
        u_pts = np.zeros_like(pts)
        log_p_x = np.zeros_like(pts)

        for i, dist in enumerate(self.active_dists):
            unifs_i = dist.cdf(pts[:, i])
            u_pts[:, i] = unifs_i
            log_p_x[:, i] = dist.logpdf(pts[:, i])

        # now pass these points through the copula ball tree KDE
        # have to calculate a bandwidth...what a nightmare
        h = 1.06 * np.std(u_pts) * u_pts.shape[0] ** (-0.2)
        log_copula_pdf = self.copula_pdf_tree.kernel_density(
            u_pts,
            h,
            return_log=True
            )

        log_p_marginal = np.sum(log_p_x, axis=-1)
        return log_copula_pdf + log_p_marginal
        

    def expect(
            self,
            f, 
            n=1000,
            ):
        """
        Compute the Monte Carlo expectation of the function f under the copula + active dists.

        :param f: function for which to compute the expectation
        :type f: `callable`
        :param n: number of draws from the copula to use, optional (default n=1000)
        :type n: `int`

        :returns: `float` the Monte Carlo expectation of the function
        """
        f_n = self.fn_dist(f, n=n)
        # (negative) infs happen because we attempt to eval ppf at 0 or 1
        f_n[f_n == np.inf] = np.nan
        f_n[f_n == -1. * np.inf] = np.nan
        return np.nanmean(f_n, axis=0)

    def fn_dist(
        self,
        f,
        n=1000,
            ):
        """
        Draw n rvs of the function f evaluated under the copula + active dists.

        Returned values may contain infs or -infs. This is because the way the copula is derived from empirical data internally can lead to a ppf being evaluated at 0 or 1, which will return -inf or inf respectively for a marginal defined on (-\infty, \infty).
        
        :param f: function for which to compute the expectation
        :type f: `callable`
        :param n: number of draws from the copula to use, optional (default n=1000)
        :type n: `int`

        :returns: `numpy.ndarray` of shape (n, p)
        """
        # draw uniforms from the copula
        u_n_inds = np.random.choice(
            self.marginal_unifs.shape[0],
            size=n,
            replace=True,
                )
        u_n = self.marginal_unifs[u_n_inds]
        # now pass these uniforms through the ppfs of marginals
        x_n = np.array(
            [dist.ppf(u_n[:, i]) for i, dist in enumerate(self.active_dists)]
                ).T
        f_n = np.array([f(x) for x in x_n])
        return f_n
        
    def reset_univariate_dists(self):
        """
        Resets the active dists to the original observed dists.
        """
        self.active_dists = self.marginal_pdfs

    def replace_univariate_dists(
            self,
            dists,
            inds=None,
            dist_args=None,
            dist_kwargs=None,
            ):
        """
        Replaced one or more of the active dists with another marginal distribution.

        :param dists: a single `scipy.stats.continuous_rv` derived object or an iterable of such objects. If an iterable, must be the same length as either inds (if inds is not None) or as the dimensionality of thee joint pdf.
        :type dists: `scipy.stats.continuous_rv` derived object or `iterable` of such objects
        :param inds: Optional, default `inds = None`. If `inds is None`, then all distributions will be replaced by the single distribution passed to dist. If `inds is not None`, then inds should be a list of integer indices. The marginal pdfs corresponding to these dimensions will be replaced.
        :type inds: `None` or `list` of `int`
        :param dist_args: Optional, default is None. Arguments to pass to distributions. If not None, must have the same length as dists.
        :type dist_args: `None` or `iterable`
        :param dist_kwargs: Optional, default is None. Keyword arguments to pass to distributions. If not None, must have the same length as dists.
        :type dist_kwargs: `None` or `iterable` of `dict`s
        """
        if inds is None:
            if not self._check_iter(dists):
                dists = [dists for _ in range(self.marginal_unifs.shape[-1])]
            if len(dists) != self.marginal_unifs.shape[-1]:
                raise ValueError(
                        'If not passing specific indices, must pass a list'
                        ' of distributions with same length as original dimension'
                        )
            # set new distributions for all active distributions
            if dist_args == None:
                dist_args = (() for _ in range(self.marginal_unifs.shape[-1]))
            elif not self._check_iter(dist_args):
                dist_args = [dist_args for _ in range(self.marginal_unifs.shape[-1])]

            if dist_kwargs == None:
                dist_kwargs = (dict() for _ in range(self.marginal_unifs.shape[-1]))
            elif not self._check_iter(dist_kwargs):
                dist_kwargs = [dist_kwargs for _ in range(self.marginal_unifs.shape[-1])]

            self.new_univariate_dists = [
                dist(*args, **kwargs)\
                        for dist, args, kwargs in zip(dists, dist_args, dist_kwargs)
                    ] 
        else:
            # set new distributions for selected indices
            # if only one dist is passed, assume that this dist id for all active dims
            if not self._check_iter(dists):
                dists = [dists for _ in inds]

            if dist_args == None:
                dist_args = (() for _ in inds)

            if dist_kwargs == None:
                dist_kwargs = (dict() for _ in inds)

            self.replaced_univariate_dists = [
                dist(*args, **kwargs)\
                        for dist, args, kwargs in zip(dists, dist_args, dist_kwargs)
                    ]

            self.new_univariate_dists = []
            j = 0
            for i, orig in enumerate(self.marginal_pdfs):
                if i not in inds:
                    self.new_univariate_dists.append(orig)
                else:
                    self.new_univariate_dists.append(self.replaced_univariate_dists[j])
                    j += 1

        self.active_dists = self.new_univariate_dists

    def _construct_marginal_pdfs(self):
        arrs = np.apply_along_axis(
            functools.partial(np.histogram, bins='auto', density=True),
            0,
            self.data,
                )
        bins, edges = arrs
        self.marginal_pdf_bins = bins
        self.marginal_pdf_edges = edges
        self.marginal_pdfs = [
            stats.rv_histogram(item)\
                for item in zip(self.marginal_pdf_bins, self.marginal_pdf_edges)
                ]

    def _construct_marginal_unifs(self):
        if self.marginal_pdfs is None:
            self._construct_marginal_pdfs()
        # iterate through dimensions, constructing uniform marginals via PIT
        self.marginal_unifs = np.zeros_like(self.data)
        for i, dist in enumerate(self.marginal_pdfs):
            unifs_i = dist.cdf(self.data[:, i])
            self.marginal_unifs[:, i] = unifs_i

    def _construct_copula_pdf(self):
        """
        Construct a ball tree used for approximating the pdf of the empirical copula
        """
        if self.marginal_unifs is None:
            self._construct_marginal_unifs()
        self.copula_pdf_tree = BallTree(
            self.data,
                ) 
